<?php
/**
 * Template Name: About Us
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>
<?php get_template_part( 'partials/about-us/info-opening-hours' ); ?>
<?php get_template_part( 'partials/about-us/gallery' ); ?>
<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>