<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
 // return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function remove_tags_and_categories() {

}

/**
 * Remove post tags and categories from the standard 'Post' post type
 */
add_action('init', function(){
    global $wp_taxonomies;
    unregister_taxonomy_for_object_type( 'category', 'post' );
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
    if ( taxonomy_exists( 'category'))
        unset( $wp_taxonomies['category']);
    if ( taxonomy_exists( 'post_tag'))
        unset( $wp_taxonomies['post_tag']);
    unregister_taxonomy('category');
    unregister_taxonomy('post_tag');
});

/**
 * Add options page for services CPT
 */
if ( function_exists( 'acf_add_options_sub_page' ) ){
    acf_add_options_sub_page(array(
        'title'      => 'Services Listing Page Settings',
        'parent'     => 'edit.php?post_type=services',
        'capability' => 'manage_options',
        'post_id'    => 'services'
    ));
}

/**
 * Add options page for team member CPT
 */
if ( function_exists( 'acf_add_options_sub_page' ) ){
    acf_add_options_sub_page(array(
        'title'      => 'Team Listing Page Settings',
        'parent'     => 'edit.php?post_type=team_members',
        'capability' => 'manage_options',
        'post_id'    => 'team_members'
    ));
}

/**
 * Add options page for case study CPT
 */
if ( function_exists( 'acf_add_options_sub_page' ) ){
    acf_add_options_sub_page(array(
        'title'      => 'Case Study Listing Page Settings',
        'parent'     => 'edit.php?post_type=case_study',
        'capability' => 'manage_options',
        'post_id'    => 'case_study'
    ));
}

add_filter( 'get_the_archive_title', function ( $title ) {
    if( is_post_type_archive() ) {
        $title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
    }
    return $title;
});

function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', __NAMESPACE__ . '\\wpdocs_custom_excerpt_length', 999 );
