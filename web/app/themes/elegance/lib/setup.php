<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size( 'full-size', '4000', '3000', false );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  // add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(Assets\asset_path('styles/main.css'));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_home(),
    is_single(),
    is_page_template('template-custom.php'),
    is_page_template('template-about-us.php'),
    is_page_template('template-service-home.php'),
    is_page_template('template-pricing.php'),
    is_page_template('template-case-study-archive.php'),
    is_page_template('template-case-study-single.php'),
    is_page_template('template-contact.php'),
    is_post_type_archive('team_members'),
    is_post_type_archive('services'),
    is_singular('services'),
  ]);

    $display = false;

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    // !!!!! Leave at top !!!!!
    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);

    // Google Fonts
    wp_enqueue_style( 'catamaran-google-font', 'https://fonts.googleapis.com/css?family=Catamaran:100,300,400', false );
    wp_enqueue_style( 'lato-google-font', 'https://fonts.googleapis.com/css?family=Lato:100,300,400', false );
    wp_enqueue_style( 'poppins-google-font', 'https://fonts.googleapis.com/css?family=Poppins:100,300,400,600,700', false );

    if (is_front_page()) {
        wp_enqueue_script('slick_script', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.min.js' , ['jquery'], null, true);
        wp_enqueue_style('slick_style', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css');
        wp_enqueue_style('slick_theme_style', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css');
    }

    if (is_page_template('template-about-us.php') || is_post_type_archive('team_members') || get_post_type() ==
        'case_study') {
        wp_enqueue_style( 'magnific-popup-css', '//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' );
        wp_enqueue_script( 'magnific-popup', '//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js',
            array(), '3', true );
    }

    if (is_home()) {
        wp_enqueue_script( 'masonry-js', '//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js');
    }

    wp_enqueue_script( 'google-map', '//maps.googleapis.com/maps/api/js?key=AIzaSyDm8tPOxh56re-yThioObzrVvKU02vvMRA', array(), '3', true );
    wp_enqueue_script( 'jquery-cookie', '//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js', array(), '', true );


    wp_enqueue_style('ionicons', get_template_directory_uri() . '/bower_components/Ionicons/css/ionicons.css');
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);

/**
 * Advanced Custom Fields - Options page
 */

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

acf_update_setting('google_api_key', 'AIzaSyDm8tPOxh56re-yThioObzrVvKU02vvMRA');
