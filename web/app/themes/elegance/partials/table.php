<?php
if ( $table ) {

    echo '<table border="0">';

    if ( $table['header'] ) {

        echo '<thead>';

        echo '<tr>';

        foreach ( $table['header'] as $th ) {

            echo '<th>';
            echo $th['c'];
            echo '</th>';
        }

        echo '</tr>';

        echo '</thead>';
    }

    echo '<tbody>';

    foreach ( $table['body'] as $tr ) {

        $empty = true;
        $cellClass = 'full-span';
        $cellCount = count($tr);
        foreach ($tr as $key => $td) {
            // First cell of row
            if ($key == 0) {
                continue;
            }
            // If empty
            if (!empty($td['c'])) {
                $empty = false;
                continue;
            }
            // Get rid of the cell
            unset($tr[$key]);
        }

        if (!$empty) {
            $cellCount = 1;
            $cellClass = '';
        }

        echo '<tr>';

        foreach ( $tr as $td ) {

            if ($td['c'] == 'Y') {
                $td['c'] = '<span class="ion-checkmark"></span>';
            } elseif ($td['c'] == 'N') {
                $td['c'] = '<span class="ion-close"></span>';
            }

            echo '<td class="' . $cellClass . '" colspan="' . $cellCount .'">';
            echo $td['c'];
            echo '</td>';
        }

        echo '</tr>';
    }

    echo '</tbody>';

    echo '</table>';
}
?>