<?php

query_posts([
    'posts_per_page' => 9,
    'post_status' => 'publish'
]);

?>

<?php if (have_posts()) : ?>
<section class="blog-listing">
    <div class="container">
        <div class="row blog-grid">
            <?php while (have_posts()) : the_post() ?>
                <?php $thumbnailUrl = get_the_post_thumbnail_url(); ?>
                <div class="col-lg-3 col-md-4 col-sm-6 blog-item-col">
                    <div class="blog-item">
                            <?php if ($thumbnailUrl) : ?>
                        <div class="blog-item-image">
                                <img src="<?= $thumbnailUrl; ?>" alt="">
                        </div>
                            <?php endif; ?>
                        <div class="blog-item-info">
                            <span class="blog-item-date"><?php the_date('d.m.Y'); ?></span>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <?php the_excerpt(); ?>
                            <div class="blog-item-link">
                                <a href="<?php the_permalink(); ?>">Read article <span class="arrow
                                ion-chevron-right pull-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
