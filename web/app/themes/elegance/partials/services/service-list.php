<?php

$featured_args = array(
    'post_type'    => 'services',
    'post_status'  => 'publish',
    'meta_key'		=> 'featured',
    'meta_value'	=> '1'
);
$featured_services = new WP_Query( $featured_args ); // Services with featured flag

$standard_args = array(
    'post_type'    => 'services',
    'post_status'  => 'publish'
);
$standard_services = new WP_Query( $standard_args ); // All services

?>

<?php if ( $featured_services->have_posts() ) :  ?>

  <div class="container services">

      <h5>Featured Services</h5>

      <div class="row">

          <?php while ( $featured_services->have_posts() ) : $featured_services->the_post(); ?>

              <?php $featured_service_image = get_field('featured_service_image')['sizes']['medium_large']; ?>

              <a class="col-md-4 service-link no-underline-border" href="<?php the_permalink(); ?>">
                  <?php if (get_field('featured_service_image')) : ?>
                      <img class="featured-image" src="<?= $featured_service_image ?>">
                  <?php endif;?>
                  <div class="service grey-border">
                      <h3 class="service-name h1"><?php the_title(); ?></h3>
                      <div class="service-excerpt grey-text"><?php the_excerpt(); ?></div>
                      <button class="btn btn-primary no-underline-border">
                          <span class="ion-arrow-right-c no-underline-border"></span>
                      </button>
                  </div>
              </a>

          <?php endwhile; ?>

      </div>
  </div>

<?php endif; ?>

<?php wp_reset_postdata(); // Restore original Post Data  ?>

<?php if ( $standard_services->have_posts() ) :  ?>

  <div class="container services">

      <h5>All Services</h5>

      <div class="row">

          <?php while ( $standard_services->have_posts() ) : $standard_services->the_post(); ?>

              <a class="col-md-4 service-link no-underline-border" href="<?php the_permalink(); ?>">
                  <div class="service grey-border opaque-grey-background">
                      <h1 class="service-name"><?php the_title(); ?></h1>
                      <div class="service-excerpt grey-text"><?php the_excerpt(); ?></div>
                      <button class="btn btn-primary no-underline-border">
                          <span class="ion-arrow-right-c no-underline-border"></span>
                      </button>
                  </div>
              </a>

          <?php endwhile; ?>

      </div>
  </div>

<?php endif; ?>

<?php wp_reset_postdata(); // Restore original Post Data  ?>