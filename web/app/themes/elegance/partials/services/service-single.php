<?php $service_image = get_field('image')['sizes']['large']; ?>

<div class="container content-wrapper">
    <div class="row">

        <div class="col-md-7">

            <?php if (get_field('image')) : ?>
                <div class="image">
                    <img src="<?= $service_image ?>">
                </div>
            <?php endif; ?>

            <div class="header">
                <h3 class="h1"><?php the_field('header'); ?></h3>
            </div>

            <div class="text">
                <?php the_field('main_text'); ?>
            </div>

        </div>

        <div class="col-md-5">

            <?php if( have_rows('advantages') ) : ?>

                <h5>Advantages of <?php the_title(); ?></h5>

                <div class="advantage-list">

                    <?php while ( have_rows('advantages') ) : the_row(); ?>
                        <ul>
                            <li class="advantage">
                                <div class="advantage-title h3">
                                    <?php the_sub_field('advantage_title'); ?>
                                </div>
                                <div class="advantage-text">
                                    <?php the_sub_field('advantage_text'); ?>
                                </div>
                            </li>
                        </ul>
                    <?php endwhile; ?>

                </div>

            <?php endif; ?>

            <div class="booking-form">

                <h3 class="header h1">Book today</h3>
                <h3 class="sub-header h3">Fill out the form below and we’ll be in touch.</h3>

                <?php

                    $form_object = get_field('contact_form');
                    $form_id = $form_object->id;
                    $form_title = $form_object->post_title;

                    if( $form_object ):
                        echo do_shortcode( '[contact-form-7 id="' . $form_id . '" title="' . $form_title . '"]' );
                    endif;

                ?>

            </div>

        </div> <!-- .col-md-5 -->
    </div> <!-- .row -->
</div> <!-- .container -->