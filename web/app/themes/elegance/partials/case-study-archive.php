<?php

$args = array('post_type' => 'case_study');
$query = new WP_Query($args);

?>

<?php if ($query->have_posts()): ?>

    <section class="case-study-listing">
        <div class="container">
            <div class="row">

                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 case-study-item-col">
                        <div class="border-box case-study-listing-item">
                            <div class="box-info">
                                <div class="box-info-text">
                                    <h3>Gallery</h3>
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                </div>
                                <div class="box-link">
                                    <a href="<?php the_permalink(); ?>">See cases <span class="arrow
                                ion-chevron-right pull-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php wp_reset_postdata(); ?>
                <?php endwhile; ?>

            </div>
        </div>
    </section>

<?php endif; ?>


