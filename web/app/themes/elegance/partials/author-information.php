<?php

$authorId = get_the_author_meta('ID');
$authorPhoto = get_avatar($authorId);
if (empty($authorPhoto)) {
    $authorPhoto = '';
}

?>

<div class="author-information">
    <div class="row">
        <div class="col-sm-1 hidden-sm-down">
            <div class="author-photo" style="background-image:url('<?= $authorPhoto; ?>');">

            </div>
        </div>
        <div class="col-xs-12 col-sm-11">
            <h5>About the author</h5>
            <h3><a href="<?= get_author_posts_url($authorId); ?>" rel="author"
                   class="fn"><?= get_the_author(); ?></a></h3>
            <p><?= get_the_author_meta('description'); ?></p>
        </div>
    </div>
</div>