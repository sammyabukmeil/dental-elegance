<?php include('utility/acf-parameters.php'); ?>

<?php if (get_field('testimonial', $post_id, $format_value)) : ?>
    <section class="testimonial-single">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 align-center">
                    <h2>What our clients have said...</h2>
                    <?php
                    $post = get_field('testimonial', $post_id, $format_value);
                    setup_postdata($post);
                    ?>
                    <p><?php the_content(); ?></p>
                    <h3><?php the_title(); ?></h3>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>