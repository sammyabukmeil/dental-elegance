<?php

include('utility/acf-parameters.php');

// Get featured image url correctly for normal templates and posts page template
$hero_image = get_the_post_thumbnail_url();
if ( is_home() ) {
    $hero_image = get_the_post_thumbnail_url(get_option('page_for_posts'));
}
if ( is_post_type_archive( 'services' ) || is_post_type_archive('team_members') || is_post_type_archive('case_study')
) {
    $hero_image = get_field('banner_image', 'options')['sizes']['large'];
    $post_id = get_post_type();
}

if (is_single()) {
    $title = get_the_title();
} else {
    $title = get_field('title', $post_id, $format_value);
}

?>

<div class="container-fluid hero-banner">
    <div class="row banner-wrapper">
        <div class="col banner-background" style="background-image: url(<?= $hero_image ?>)">
            <div class="breadcrumbs"><?php custom_breadcrumbs(); ?></div>
            <div class="centered-content">
                <div class="icon">
                    <img src="<?php the_field('icon', $post_id, $format_value); ?>">
                </div>
                <div class="header">
                    <h1><?= $title; ?></h1>
                </div>
                <?php if (get_field('subtitle', $post_id)) : ?>
                    <div class="sub-header">
                        <h2><?php the_field('subtitle', $post_id, $format_value); ?></h2>
                    </div>
                <?php endif; ?>
                <?php if(get_field('further_information')) : ?>
                    <div class="further-information">
                        <p><?php the_field('further_information', $post_id, $format_value); ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (get_field('button_text', $post_id, $format_value) && get_field('button_link', $post_id, $format_value)) : ?>
            <div class="book-button">
                <a class="btn button primary-colours" href="<?php the_field('button_link', $post_id, $format_value) ?>">
                    <?php the_field('button_text', $post_id, $format_value); ?><span class="arrow ion-arrow-right-c"></span>
                </a>
          </div>
        <?php endif; ?>
    </div>
</div>