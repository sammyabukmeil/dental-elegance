<?php if( have_rows('logos', 'option') ):  ?>
    <div class="container award-logos">
        <div class="row">
            <?php while ( have_rows('logos', 'option') ) : the_row(); ?>
                <div class="logo-container col-sm-4">
                    <img class="award-logo" src="<?php the_sub_field('logo', 'option'); ?>">
                </div>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>