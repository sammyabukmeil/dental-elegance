<div class="welcome-book info-hours container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="sub-header"><?php the_field('information_title', false. false) ?></h2>
            <p><?php the_field('information_text', false. false) ?></p>
        </div>
        <div class="col-md-6">
            <h2 class="sub-header"><?php the_field('opening_hours_title', false. false) ?></h2>
            <div class="container opening-hours">
                <div class="row">
                    <div class="col-xs-4 hours">
                        <div class="days"><?php the_field('hours_1_title', false. false) ?></div>
                        <div class="times"><?php the_field('hours_1_times', false. false) ?></div>
                    </div>
                    <div class="col-xs-4 hours">
                        <div class="days"><?php the_field('hours_2_title', false. false) ?></div>
                        <div class="times"><?php the_field('hours_2_times', false. false) ?></div>
                    </div>
                    <div class="col-xs-4 hours">
                        <div class="days"><?php the_field('hours_3_title', false. false) ?></div>
                        <div class="times"><?php the_field('hours_3_times', false. false) ?></div>
                    </div>
                </div>
            </div>
            <p><?php the_field('hours_further_information', false. false) ?></p>
        </div>
    </div>
</div>
