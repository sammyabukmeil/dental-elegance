<section class="container image-gallery">
  <?php if (get_field('gallery_title')) : ?>
    <h2 class="gallery-title large"><?php the_field('gallery_title') ?></h2>
  <?php endif; ?>

  <?php $images = get_field('gallery', 23); // Gallery page use the gallery on about us page ?>

  <?php if ($images) : ?>
      <div class="popup-gallery">
          <?php foreach( $images as $image ): ?>
              <a href="<?php echo $image['url']; ?>"
                 class="lightbox-link"
                 title="<?php echo $image['caption']; ?>"
                 data-description="<?php echo $image['description']; ?>">
                  <div class="image-wrap">
                      <img src="<?php echo $image['url']; ?>">
                  </div>
            </a>
          <?php endforeach; ?>
      </div>
  <?php endif; ?>

</section>
