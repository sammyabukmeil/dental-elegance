<?php
// Script to get ACF functions working on Posts/Blog Archive page - Included at the top of partials

// Default values for parameters of ACF functions
$format_value = true;
$post_id = false;

// If on the posts page, set the extra parameters of ACF functions
if (is_home()) {
    $format_value = $post_id = get_option('page_for_posts');
}
if ( is_post_type_archive( 'services' ) || is_post_type_archive('team_members') ) {
    $post_id = 'options';
}