<?php if( have_rows('services') ) : ?>
<div class="service-ctas container-fluid flex-md-nowrap">
    <div class="row">
        <?php while ( have_rows('services') ) : the_row(); ?>
        <a class="service-wrapper col-md-4 col-sm-12" href="<?php the_sub_field('link') ?>">
            <div class="service">
                <div class="icon-wrapper">
                    <img src="<?php the_sub_field('icon'); ?>">
                </div>
                <div class="text-wrapper">
                    <h2 class="header"><?php the_sub_field('title'); ?></h2>
                    <p class="subtitle"><?php the_sub_field('text'); ?></p>
                </div>
              <div class="arrow-wrapper">
                  <i class="arrow ion-chevron-right"></i>
              </div>
            </div>
        </a>
        <?php endwhile; ?>
    </div> <!-- .row -->
</div> <!-- .services-ctas -->
<?php endif; ?>
