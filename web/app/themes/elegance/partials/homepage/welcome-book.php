<div class="welcome-book container">
  <div class="row">
    <div class="col-md-6">
      <h1 class="header"><?php the_field('welcome_title'); ?></h1>
      <h2 class="sub-header"><?php the_field('welcome_subtitle', false, false); ?></h2>
      <p><?php the_field('welcome_text'); ?></p>

      <?php if (get_field('weclome_button_link') && get_field('welcome_button_text')) : ?>
        <a class="btn button primary-colours" href="<?php the_field('weclome_button_link'); ?>">
          <?php the_field('welcome_button_text'); ?><span class="arrow ion-arrow-right-c"></span>
        </a>
      <?php endif; ?>

    </div>
    <div class="booking-form col-md-6">
      <h1 class="header"><?php the_field('booking_form_title'); ?></h1>
      <h2 class="sub-header"><?php the_field('booking_form_subtitle'); ?></h2>

      <?php
      $form_object = get_field('contact_form');
      $form_id = $form_object->id;
      $form_title = $form_object->post_title;

      if( $form_object ):
          echo do_shortcode( '[contact-form-7 id="' . $form_id . '" title="' . $form_title . '"]' );
      endif;
      ?>

    </div>
  </div>
</div>
