<?php if( have_rows('carousel_slides') ): ?>
    <div class="carousel main">
        <?php
        while ( have_rows('carousel_slides') ) : the_row();
        $slider_background = get_sub_field('background_image')['sizes']['full-size'];
        ?>
            <div style="background-image: url(<?= $slider_background ?>)">
              <div class="container wrapper">
                <h3><?php the_sub_field('header_first_line'); ?></h3>
                <h1 class="large"><?php the_sub_field('header_second_line'); ?></h1>
                <a class="btn button primary-colours" href="<?php the_sub_field('button_link'); ?>">
                    <?php the_sub_field('button_text'); ?><span class="arrow ion-arrow-right-c"></span>
                </a>
              </div>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>