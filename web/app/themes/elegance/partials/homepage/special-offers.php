<?php

// WP_Query arguments
$args = array(
    'post_type'              => array( 'special_offers' ),
    'post_status'            => array( 'publish' ),
);

// The Query
$query = new WP_Query( $args );

?>

<?php if ( $query->have_posts() ) :  ?>
<section class="special-offers-section">
    <div class="container special-offers">
        <h2 class="section-header large"><?php the_field('section_title') ?></h2>
        <h3 class="section-sub-header"><?php the_field('section_subtitle') ?></h3>
        <div class="row offers-container">

        <?php while ( $query->have_posts() ) : ?>
            <?php $query->the_post(); ?>

                <div class="offer-box col-md-4">
                    <div class="contents-wrapper">
                        <img class="offer-img" src="<?php the_post_thumbnail_url() ?>">
                        <div class="text-wrapper">
                            <p class="service"><?php the_title() ?></p>
                            <h2 class="offer-title"><?php the_field('offer') ?></h2>
                            <p class="offer-details"><?php the_field('offer_details') ?></p>
                            <p class="contact-info"><?php the_field('contact_information') ?></p>
                        </div>
                    </div>
                </div>

        <?php endwhile; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?php wp_reset_postdata(); // Restore original Post Data  ?>