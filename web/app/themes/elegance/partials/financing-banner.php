<?php if (get_field('banner_title', 'option' )) : ?>
    <section class="container-fluid finance-banner">
        <div class="container inner-container">
            <div class="row">
                <div class="col-md-8 col-xs-12 title-wrapper">
                    <h2><?php the_field('banner_title', 'option') ?></h2>
                </div>
                <div class="col-md-4 col-xs-12 button-wrapper">
                    <a class="btn button primary-colours" href="<?php the_field('finance_banner_button_link', 'option') ?>">
                        <?php the_field('finance_banner_button_text', 'option') ?><span class="arrow ion-arrow-right-c"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>