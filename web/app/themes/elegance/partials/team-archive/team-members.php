<?php

$args = array('post_type' => 'team_members');
$query = new WP_Query( $args );

?>

<?php if( $query->have_posts() ): ?>

    <section class="container team-members">
        <div class="row">

        <?php while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php
            $split_name = explode(' ', trim( get_the_title() ));
            $first_name = $split_name[0];
            ?>

            <div class="col-lg-4 col-md-6 team-member">

                <div class="team-member-inner">
                    <a href="#team-member-popup" class="team-member-link open-popup-link">
                        <div class="image-wrapper">
                            <img src="<?php the_post_thumbnail_url(); ?>">
                        </div>
                        <div class="info-wrapper">
                            <div class="name-and-position">
                                <h2 class="name"><?php the_title(); ?></h2>
                                <h3 class="position"><?php the_field('position'); ?></h3>
                            </div>
                            <div class="about-member">
                                <p class="about-link">About <?= $first_name ?>
                                    <span class="arrow ion-chevron-right"></span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>


                <!-- The content here is transfered to the modal below via javascript on click -->
                <div class="modal-content hidden">
                    <div class="image-src"><?php the_post_thumbnail_url(); ?></div>
                    <div class="name"><?php the_title(); ?></div>
                    <div class="name-prefix"><?php the_field('name_prefix') ?></div>
                    <div class="position"><?php the_field('position') ?></div>
                    <div class="gdc-number"><?php the_field('gdc_number') ?></div>
                    <div class="biography"><?php the_content(); ?></div>
                </div>

            </div>

        <?php endwhile; ?>

        </div>
    </section>

    <!-- Popup Modal - Content fetched from hidden div .modal-content above on click -->
    <div id="team-member-popup" class="white-popup mfp-hide">
        <div class="team-member-inner">
            <div class="container">
                <div class="row">
                    <div class="hidden-sm-down col-md-3 image-wrapper">
                        <img class="profile-image" src="">
                    </div>
                    <div class="col-xs-12 col-md-9 info-wrapper">
                        <div class="name-and-position">
                            <h2 class="name"></h2><span class="h1 name-prefix"></span>
                            <h3 class="position">
                                <span class="position-container"></span>
                                <span class="GDC-container">GDC No. <span class="GDC"></span></span>
                            </h3>
                        </div>
                        <div class="biography">
                          <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>
