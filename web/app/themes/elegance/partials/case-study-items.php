<?php

?>

<?php if (have_rows('case')) : ?>
<section class="case-study-items">
    <div class="container">
        <div class="row">
            <?php while (have_rows('case')) : the_row();?>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="case-study-item">
                        <div class="case-study-thumbnails clearfix">
                            <div class="before thumbnail" style="background-image: url('<?php the_sub_field
                            ('thumbnail_before'); ?>')
                            ;">
                            </div>
                            <div class="after thumbnail" style="background-image: url('<?php the_sub_field('thumbnail_after');
                            ?>"></div>
                            <span class="circle ion-ios-search-strong"></span>
                        </div>
                        <div class="case-study-modal-content mfp-hide mfp-with-anim">
                            <div class="image">
                                <img src="<?php the_sub_field('gallery_image'); ?>" alt="">
                            </div>
                            <div class="info">
                                <?php the_sub_field('description') ;?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>