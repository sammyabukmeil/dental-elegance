/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Elegance = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        // Only on stqging
        if(window.location.href.indexOf("prismweave") > -1) {
          // Elegance.common.functions.removeHrefs();
         // Elegance.common.functions.disableLinkClicking();
          Elegance.common.functions.disableFormSubmit();
          // Elegance.common.functions.disableRightClick();
        }

        if ( $(window).width() < 1200 ) {
          Elegance.common.functions.openMobileMenu();
          Elegance.common.functions.showSubmenu();
          Elegance.common.functions.goBackToPreviousMenu();
          Elegance.common.functions.addSubMenuArrows();
        }

        Elegance.common.functions.preventTabbingMobileMenu();
        Elegance.common.functions.closePromotionBanner();
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      },
      functions: {
        disableLinkClicking: function() {
          // Disable clicking any anchor links
          $('a').bind("click.myDisable", function() { return false; });
        },
        disableFormSubmit: function() {
          // Disable submitting any forms
          $('input[type=submit]').attr('disabled', 'disabled');
        },
        removeHrefs: function() {

          var validLinks = [
            'http://dental-elegance.prismweave.com/about-us/',
            'http://dental-elegance.prismweave.com/contact/',
            'http://dental-elegance.prismweave.com/about-us/team/',
            'http://dental-elegance.prismweave.com/pricing/',
            'http://dental-elegance.prismweave.com/blog/'
          ];
          // Remove hrefs so link info box (bottom left) doesn't show page name
          $('a').each(function() {
            if ( $.inArray($(this).attr('href'), validLinks) !== -1
            || $(this).is('.lightbox-link, .brand, .open-popup-link')) {
              return true;
            }
            if ($(this).parent().parent().hasClass('blog-item-info')) {
              return true;
            }
            $(this).attr('href', '');
            $(this).bind("click.myDisable", function() { return false; });
          });

        },
        disableRightClick: function() {
          // Disable right clicking so client can't open link in new tab
          document.addEventListener("contextmenu", function(e){
            e.preventDefault();
          }, false);
        },
        openMobileMenu: function() {
          //Slide the mobile nav menu in/out on click
          $('.navbar-toggler').click(function() {
            if (!$('.mobile-nav').hasClass('open')) {
              $('.main-wrapper').addClass('menu-open');
              $(this).children('span').attr('class', 'ion-arrow-right-c');
              $('.mobile-nav').addClass('open').removeClass('closed');
            } else {
              $('.main-wrapper').removeClass('menu-open');
              $(this).children('span').attr('class', 'ion-navicon');
              $('.mobile-nav').removeClass('open').addClass('closed');
            }
          });
        },
        showSubmenu: function() {
          // Clicking on a menu item with sub menu nested inside slides the sub menu into view
          $('.menu-item-has-children').children('a').on('click', function(event) {
            var selected = $(this),
              selectedText = selected.text(), // The text of the clicked link used to create the back button
              selectedLink = $(this).attr('href');

            event.preventDefault();

            // If the menu item has a sub menu, slide it into view and slide the first level menu out of view
            if (selected.next('ul').hasClass('sub-menu')) {
              // Add and remove classes
              selected.addClass('selected').next('ul').addClass('is-visible').end().parent('.menu-item-has-children')
                .parent('ul').addClass('moves-out').removeClass('is-visible');
              selected.parent('.menu-item-has-children').siblings('.menu-item-has-children').children('ul').addClass('is-hidden')
                .end().children('a').removeClass('selected');

              // Create a link to go back a level if there isn't one already
              if (!$(selected.siblings('ul')).find('.go-back').length) {
                selected.siblings('ul').prepend("<li><a href='" + selectedLink + "'>" + selectedText + "</span></a></li>");
                selected.siblings('ul').prepend("<li class='go-back'><a href='#0'>Back<span class='arrow ion-arrow-right-c'></span></a></li>");
              }
            }
          });
        },
        goBackToPreviousMenu: function() {
          // Clicking on a go back link takes you back a level
          $('.sub-menu').delegate('.go-back','click', function() {
            $(this).parent('ul').removeClass('is-visible').parent('.menu-item-has-children').parent('ul').removeClass('moves-out');
            $(this).parents('.menu-item-has-children').siblings('.menu-item-has-children').children('ul').removeClass('is-hidden');
          });
        },
        addSubMenuArrows: function() {
          // Add arrows to show submenus and back links
          $('.mobile-nav li.menu-item-has-children > a').append('<span class="arrow ion-arrow-right-c"></span>');
          $('.mobile-nav li.go-back > a').prepend('<span class="arrow ion-arrow-left-c"></span>');
        },
        preventTabbingMobileMenu: function() {
          // Tabbing to mobile menu elements causes the page to break
          $('.mobile-nav a, .mobile-nav button').attr('tabindex', '-1');
        },

        renderGoogleMap: function(config) {
          var mapObj = $('#map-object')[0],
            map = new google.maps.Map(mapObj, config);

          this.addMarker(config.center ,map);
        },
        addMarker : function( latLngConfig, map ) {
      
          // var
          var latLng = new google.maps.LatLng( latLngConfig.lat, latLngConfig.lng );
      
          // create marker
          var marker = new google.maps.Marker({
            position	: latLng,
            map			: map
          });
        },

        closePromotionBanner : function() {
          $('.promotion-banner .dismiss a').click(function(e) {
            Cookies.set('dismissPromotionBanner', 'true', { expires: 7 });
            e.preventDefault();
            $('.promotion-banner').hide();
          });
        }
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
        Elegance.home.functions.initCarousels();
        Elegance.home.functions.contactFormChevron();
      },
      functions: {
        initCarousels: function() {
            $('.carousel')
              .on('init', function() {
                $('.carousel').css("visibility", "visible"); // When fully loaded, show the carousel
              })
              .slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                nextArrow: '<i class="slick-next ion-chevron-right"></i>',
                prevArrow: '<i class="slick-prev ion-chevron-left"></i>',
                responsive: [
                  {
                    breakpoint: 767,
                    settings: {
                      arrows: false
                    }
                  }
                ]
            });
            $('.testimonial-carousel').slick({
              centerMode: true,
              centerPadding: '-10px',
              infinite: true,
              slidesToShow: 3,
              arrows: true,
              dots: false,
              nextArrow: '<i class="slick-next ion-chevron-right"></i>',
              prevArrow: '<i class="slick-prev ion-chevron-left"></i>',
              responsive: [
                {
                  breakpoint: 767,
                  settings: {
                    arrows: false,
                    slidesToShow: 1,
                  }
                }
              ]
            });
          $('.offers-container').slick({
            mobileFirst: true,
            autoplay: true,
            autoplaySpeed: 5000,
            nextArrow: '<i class="slick-next ion-ios-arrow-right"></i>',
            prevArrow: '<i class="slick-prev ion-ios-arrow-left"></i>',
            responsive: [
              {
                breakpoint: 767,
                settings: "unslick"
              }
            ]
          });
        },
        contactFormChevron: function() {
          var chevron = '<span class="chevron ion-chevron-down"></span>';
          $('form.wpcf7-form .interested-in').append(chevron);
        }
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        Elegance.about_us.functions.initialisePopupGallery();
      },
      functions: {
        initialisePopupGallery: function() {
          $('.popup-gallery').magnificPopup({
            delegate: 'a',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
              enabled: true,
              navigateByImgClick: true,
              preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
              tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
              titleSrc: function(item) {
                return '<h4 class="lb-title">' + item.el.attr('title') + '</h4>' + '<p class="lb-description">' + item.el.attr('data-description') + '</p>';
              }
            }
          });
        }
      }
    },
    // Team page
    'post_type_archive_team_members': {
      init: function() {
        Elegance.post_type_archive_team_members.functions.initiateMagnificPopup();
        Elegance.post_type_archive_team_members.functions.popupContentReplacement();
      },
      functions: {
        initiateMagnificPopup: function() {
          $('.open-popup-link').magnificPopup({
            type:'inline',
            midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
          });
        },
        popupContentReplacement: function() {
          // Replace content of modal with that stored inside hidden div .modal-content
          $('.team-member-link').click(function() {
            var imageSrc = $(this).parent().siblings('.modal-content').find('.image-src').html(),
                name = $(this).parent().siblings('.modal-content').find('.name').html(),
                namePrefix = $(this).parent().siblings('.modal-content').find('.name-prefix').html(),
                gdcNumber = $(this).parent().siblings('.modal-content').find('.gdc-number').html(),
                position = $(this).parent().siblings('.modal-content').find('.position').html(),
                biography = $(this).parent().siblings('.modal-content').find('.biography').html(),
                gdcContainer = $('.GDC-container'); 

            $('.white-popup .profile-image').attr('src', imageSrc);
            $('.white-popup .biography').html(biography);
            $('.white-popup .name').html(name);
            $('.white-popup span.name-prefix').html(namePrefix);
            $('.white-popup span.position-container').html(position);
            $('.white-popup span.GDC').html(gdcNumber);

            // If not GDC number found, hide its container
            if (!gdcNumber.length) {
              gdcContainer.hide();
            } else {
              gdcContainer.show();
            }
          });
        }
      }
    },
    // Team page
    'contact': {
      init: function() {
        // Setup map
        var centerLatLng = {lat: 51.5074, lng: 0.1278},
            zoom = 17;
        if (typeof mapConfig !== 'undefined' && mapConfig) {
          centerLatLng = {
            lat: parseFloat(mapConfig.lat),
            lng: parseFloat(mapConfig.lng)
          };
        }
        var mapParams = {
          center: centerLatLng,
          zoom: zoom,
          streetViewControl: true
        };
        console.log(mapParams);
        $(window).load(function() {
          Elegance.common.functions.renderGoogleMap(mapParams);
        });
      },
      functions: {
      }
    },
    // Team page
    'blog': {
      init: function() {
        $('.blog-grid').masonry({
          itemSelector: '.blog-item-col',
          percentPosition: true
        });
      },
      functions: {
        
      }
    },
    'single_case_study' : {
      init: function() {
        $('.case-study-thumbnails').click(function() {
          var content = $(this).siblings('.case-study-modal-content');
          $.magnificPopup.open({
            items: {
              src: content,
              type:'inline',
              midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
            },
            removalDelay: 220, //delay removal by X to allow out-animation
            callbacks: {
              beforeOpen: function() {
                this.st.mainClass = 'mfp-zoom-in';
              }
            }
          });
        });
      },
      functions: {
      }
    },
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Elegance;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
