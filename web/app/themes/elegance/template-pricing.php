<?php
/**
 * Template Name: Pricing
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>
<?php if (get_field('membership_fees_table')): ?>
<section class="membership-fees">
    <div class="container">
        <h5 class="border-bottom">Membership Price Table</h5>
    <?php if (get_field('membership_fees_table_title')): ?>
        <h2 class="border-line"><?php the_field('membership_fees_table_title'); ?></h2>
    <?php endif; ?>

<?php $table = get_field( 'membership_fees_table' ); ?>
<?php include('partials/table.php'); ?>
        
    <?php if (get_field('membership_fees_table_title')): ?>
        <div><?php the_field('membership_fees_information'); ?></div>
    <?php endif; ?>
    </div>
</section>
<?php endif;?>

<section class="fees">
    <div class="container">

        <?php $table = get_field( 'fees_table' ); ?>
        <?php include('partials/table.php'); ?>

    </div>
</section>

<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>