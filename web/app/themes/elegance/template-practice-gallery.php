<?php
/**
 * Template Name: Practice Gallery
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>

<?php get_template_part( 'partials/about-us/gallery' ); ?>

<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>
