<footer class="content-info">
    <div class="container">
        <div class="row footer-top">
            <div class="col-md-2"><?php the_field('footer_logo', 'option'); ?></div>
            <div class="col-md-2"><?php the_field('footer_box_1', 'option'); ?></div>
            <div class="col-md-2"><?php the_field('footer_box_2', 'option'); ?></div>
            <div class="col-md-4"><?php the_field('footer_box_3', 'option'); ?></div>
            <div class="col-md-2">
                <div class="social-icons">
                    <div>
                        <a href="<?php the_field('facebook_link', 'options') ?>">
                            <div class="circle"><span class="ion-social-facebook"></span></div>
                        </a>
                    </div>
                    <div>
                        <a href="<?php the_field('twitter_link', 'options') ?>">
                            <div class="circle"><span class="ion-social-twitter"></span></div>
                        </a>
                    </div>
                    <div>
                        <a href="<?php the_field('linkedin_link', 'options') ?>">
                            <div class="circle"><span class="ion-social-linkedin"></span></div>
                        </a>
                    </div>
                </div>
            </div>
            <?php dynamic_sidebar('sidebar-footer'); ?>
        </div>
        <div class="line"></div>
        <div class="row">
            <div class="col-xs-6">
                <div class="footer-copyright">
                    <span>Copyright 2016-<?= date("Y"); ?> Dental Elegance</span>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="footer-legal-links align-right">
                    <a href="/about-us/policies/">Complaints Policy</a>
                    <a href="/">Terms & Conditions</a>
                    <a href="/">Sitemap</a>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<nav class="mobile-nav closed">
    <?php
    if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
    endif;
    ?>
    <button class="btn button secondary-colours">Book now</button>
    <div class="contact-number">
        <span class="ion-ios-telephone">0208 850 8820</span>
    </div>
</nav>