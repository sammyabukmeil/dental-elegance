<?php if (have_rows('testimonial_slides')): ?>
    <section class="testimonial-section">
        <div class="container">
            <div class="pull-xs-right all-testimonials-btn">
                <a href="/" class="button btn primary-colours">All testimonials<span class="arrow ion-arrow-right-c"></span></a>
            </div>
            <div class="testimonial-carousel">
                <?php
                while (have_rows('testimonial_slides')) : the_row();
                    $post = get_sub_field('testimonial');
                    if ($post) :
                        setup_postdata($post); ?>
                    <div class="testimonial-item content-wrapper">
                        <div class="photo">
                            
                        </div>
                        <?php the_content(); ?>
                        <h3 class="testimonial-name"><?php the_title(); ?></h3>
                    </div>
                    <?php wp_reset_postdata();
                     endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>