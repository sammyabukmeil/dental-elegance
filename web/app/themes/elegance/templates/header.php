<?php

$logo = get_field('main_logo', 'option')['sizes']['large'];
$phone_number = get_field('telephone_number', 'options');
$stripped_phone_number = str_replace(' ', '', $phone_number);

// Start of main wrapper. Ends in FOOTER. For mobile nav menu
?>

<?php if ( !isset($_COOKIE['dismissPromotionBanner']) ) : ?>

<div class="promotion-banner container-fluid">
    <div class="row">

        <div class="col-xs-2"></div>

        <div class="col-xs-8 text-wrapper">
            <div class="text">
                <?php the_field('promotional_text', 'option') ?>
            </div>
            <a href="<?php the_field('button_link', 'option') ?>">
                <button class="btn btn-primary button">
                    <?php the_field('button_text', 'option') ?>
                </button>
            </a>
        </div>

        <div class="col-xs-2 dismiss-wrapper">
            <div class="dismiss">
                <a href="#">
                    <span class="ion-close-round"></span>
                </a>
            </div>
        </div>

    </div>
</div>

<?php endif; ?>

<div class="main-wrapper">
<header class="banner">
  <div class="container wrapper">
    <a class="brand col-md-3" href="<?= esc_url(home_url('/')); ?>"><img src="<?= $logo ?>"></a>
    <nav class="nav-primary col-md-6">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif;
      ?>
    </nav>
    <div class="call-to-actions col-md-3">
      <div class="social-icons">
        <a href="<?php the_field('facebook_link', 'options') ?>">
          <div class="circle"><span class="ion-social-facebook"></span></div>
        </a>
        <a href="<?php the_field('twitter_link', 'options') ?>">
          <div class="circle"><span class="ion-social-twitter"></span></div>
        </a>
        <a href="<?php the_field('linkedin_link', 'options') ?>">
          <div class="circle"><span class="ion-social-linkedin"></span></div>
        </a>
      </div>
      <div class="contact-number">
        <a class="ion-ios-telephone"
           href="tel:<?= $stripped_phone_number ?>">
            <?php the_field('telephone_number', 'options'); ?>
        </a>
      </div>
      <a href="<?php the_field('book_button_link', 'options') ?>" class="btn button secondary-colours">
        Book now
      </a>
    </div>
    <button class="navbar-toggler" type="button"><span class="ion-navicon"></span></button>

</header>


  


