<?php while (have_posts()) : the_post(); ?>
    <div class="container">
  <article <?php post_class(); ?>>
    <header>
      <time class="updated" datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <?php get_template_part( 'partials/author-information' ); ?>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
    </div>
<?php endwhile; ?>
