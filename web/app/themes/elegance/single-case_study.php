<?php
/**
 * Template Name: Case Study Single
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>
<?php get_template_part( 'partials/case-study-items' ); ?>
<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>