<?php
/**
 * Template Name: Policies
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>

<?php get_template_part( 'partials/homepage/welcome-book' ); ?>

<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>
