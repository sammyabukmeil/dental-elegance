<?php get_template_part( 'partials/homepage/carousel'); ?>
<?php get_template_part( 'partials/homepage/service-ctas' ); ?>
<?php get_template_part( 'partials/homepage/welcome-book' ); ?>
<?php get_template_part( 'partials/homepage/special-offers' ); ?>
<?php get_template_part( 'partials/awards' ); ?>
<?php get_template_part( 'templates/testimonial', 'carousel' ); ?>
