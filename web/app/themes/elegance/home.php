<?php
/**
 * Template Name: Blog Archive
 */
?>

<?php get_template_part( 'partials/hero-banner' ); ?>
<?php get_template_part( 'partials/blog-listing' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>