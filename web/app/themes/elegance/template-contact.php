<?php
/**
 * Template Name: Contact
 */
?>

<section class="blue-dark contact">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 align-center introduction">
                <?php if (get_field('icon'))  : ?>
                <div class="icon">
                    <img src="<?php the_field('icon'); ?>" alt="">
                </div>
                <?php endif; ?>
                <h1><?php the_field('title'); ?></h1>
                <h3><?php the_field('introduction'); ?></h3>
            </div>
            <div class="col-md-1 hidden-md-down"></div>
            <div class="col-lg-6 col-md-8 form-wrapper">
                <h5>Contact form</h5>
                <?php
                $form_object = get_field('contact_form');
                $form_id = $form_object->id;
                $form_title = $form_object->post_title;

                if( $form_object ):
                    echo do_shortcode( '[contact-form-7 id="' . $form_id . '" title="' . $form_title . '"]' );
                endif;
                ?>
            </div>
            <div class="col-md-1 hidden-md-down"></div>
            <div class="col-md-4">
                <?php if( have_rows('information') ):  ?>
                    <?php while ( have_rows('information') ) : the_row(); ?>
                        <h5 class="information-title">
                            <?php the_sub_field('title'); ?>
                        </h5>
                        <div class="information-content">
                            <?php the_sub_field('content'); ?>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="map">
    <div class="container">
        <div class="row">
            <div class="map-wrapper">
                <script type="text/javascript">
                    var mapConfig = <?= json_encode(get_field('map')); ?>;
                </script>
                <div id="map-object">
                </div>
            </div>
            <div class="align-center cta">
                <h2 class="large">Make your reservation today</h2>
                <a href="/" class="btn button secondary-colours">Book Now</a>
            </div>
        </div>
    </div>
</section>

<?php get_template_part( 'partials/testimonial-single' ); ?>
<?php get_template_part( 'partials/financing-banner' ); ?>
<?php get_template_part( 'partials/awards' ); ?>